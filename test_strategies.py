'''
 test_strategies.py

 @ticket:	
 @date:		2023-10-05
 @auth:		Daniel < dstratti1@gmail.com >

 @desc: 
'''
from os import path
import argparse
import warnings
from datetime import datetime

from environment import Environment
from sampler import Sampler


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import math
import json

# from sklearn.linear_model import ElasticNet, enet_path
# from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
# from sklearn.model_selection import GridSearchCV

# Import mlflow
import mlflow
import mlflow.data
from mlflow.data.pandas_dataset import PandasDataset

# --------------------------------------------------------------------------
# --------------------------------------------------------------------------
# ----------------------- Setup Arguments -----------------------
# --------------------------------------------------------------------------
# --------------------------------------------------------------------------
parser = argparse.ArgumentParser(
    prog='Hypothesis Test Random Selection Vs Thompson Sampling',
    description='This program performs a simulation to assess the cost effectiveness of ',
    epilog='''
    This program performs base line comparisons of thompson sampling vs random 
    selection for a multi armed bandit problem. The program allows the 
    specification of the amount you're willing to spend based on the cost of the
    strategies & reward.

    To load the strategies, the program expects the file XXXXXX. This file 
    should contain the "true rate of success", in this example it is the 
    `conversion_rate` column, the `cost` of selecting this strategy & the 
    `reward` given if the strategy is successful .In addition you can create a 
    sample environment based off of the conversion rates given in each strategy.

    This allows for a high number of rounds in a scenario similar to bootstrapping.

    In addition to exploring the sampling techniques, the distributions learnt 
    from the sampling are plotted and saved as an artifact.''')
parser.add_argument('-f', '--file', 
    help='Specify the JSON file containing the strategies',
    required=True)
parser.add_argument('-N', '--simulations',
    type=int,
    const=10_000,
    help="The number of simulated trails to create, this is the upper bound for any sampling method")
parser.add_argument('-s', '--stop',
    type=str,
    choices=['N', 'R', 'ROI'],
    const='N',
    help="Decides if running `N` simulations, or if using early stopping based \
         off regret `R` or `ROI` based costs")
parser.add_argument('-d', '--spend',
    type=int,
    help="Either the dollar amount or the conversion amount you are willing to loose")
parser.add_argument('-ts', '--eval-ts',
    action="store_true",
    const=True,
    help="Stoping is focused on thompson sampling metrics or if false, the random sampling metrics")


# parser.add_argument('-p', '--project',
#     help="Tag the name of the related project")


# Setup Ml-Flow Server (Docker host is on 127.0.0.1:8080)
mlflow.set_tracking_uri("http://127.0.0.1:8080")




def validate_args(self, args):
    '''
    Validate the input arguments are as expected

    file: str
     Check type and that the file exists, 

    sim: int
     Check int 
    '''
    assert isinstance(args.file, str), 'Ensure the file input is a string'
    assert path.isfile(args.file), 'Ensure the file input exists and is a file'

    if args.simulations is not None:
        assert isinstance(args.simulations, int), "Ensure the number of simulations is given as an int"

    if args.stop in ['R', 'ROI']:
        assert args.spend is not None, 'Please set the spend when using the reward (R) or ROI stoping modes'
        assert isinstance(args.spend, int), "Ensure the spend is given as an int"



# Plot 9 Beta distribution, each representing either a progression in success of failures

def beta_dist(alpha, beta):
    '''
    Create a number line of 1000 datum representing the beta distribution given 
    a number of success / failed trails

    alpha : int
        Number of successes plus 1 as it cannot be 0

    beta : int
        Number of failures plus 1 as it cannot be 0
    '''
    c = 1 / ((math.factorial(alpha) * math.factorial(beta)) / (math.factorial(alpha + beta)))
    p_sup = alpha - 1
    p_msup = beta - 1
    
    dist = []
    for x in np.arange(0, 1, step=0.001):
        dist.append(c * (x**p_sup) * ((1 - x)**p_msup))
        
    return np.array(dist)


## TODO: make F test comparrison of means
#

def plot_beta_dists(sampler, mlf):
    '''
    Plot beta distribution for each strategy

    sampler : Sampler
        The sample object used to run the simulation

    mlf : Ml-Flow
        ML-Flow object to log outputs
    '''
    # Initialise the subplot function using number of rows and columns 
    figure, axis = plt.subplots(3, 3, figsize=(15, 15)) 

    for i in range(axis.shape[0]):
        for j in range(axis.shape[1]):

            if sampler.numbers_of_rewards_1_ts is not None and sampler.numbers_of_rewards_0_ts is not None:
                r_alpha = sampler.numbers_of_rewards_1_ts[(i*3) + j]
                r_beta = sampler.numbers_of_rewards_0_ts[(i*3) + j]
            else:
                r_alpha = np.random.randint(1, 10)
                r_beta = np.random.randint(1, 10)
            
            try:
                # Create standardised distribution (y [0, 1])
                dist = beta_dist(r_alpha+1, r_beta+1)

            except Exception as e:
                print(e)
                print(f"Alpha: {r_alpha+1}, Beta: {r_beta+1}")
            #dist = (dist - dist.min()) / (dist.max() - dist.min())
            size = len(dist)
            max_i = np.argmax(dist)
            max_v = np.max(dist)

            # lower 17.5% bound & upper 82.5% bound for the middle 65% of the distribution
            lb_p = 0.175
            ub_p = (1 - lb_p)
            cum_dist = dist.cumsum()
            map_dist = (cum_dist - cum_dist.min()) / (cum_dist.max() - cum_dist.min())

            # Get stats
            m_idx = np.where(map_dist <= 0.5)[0].max()
            lb_idx = np.where(map_dist < lb_p)[0].max()
            ub_idx = np.where(map_dist > ub_p)[0].min()

              
            # Name plot based off of strategy name
            axis[i, j].plot(dist) 
            axis[i, j].set_title(f"Beta-dist {sampler.env.strategies[(i*3) + j]['name']} ({r_alpha}, {r_beta})") 
      

            # only one line may be specified; full height
            axis[i, j].axvline(x=lb_idx, color='b', label='lower_bound_65_percentile')
            axis[i, j].axvline(x=ub_idx, color='b', label='upper_bound_65_percentile')

            axis[i, j].axvline(x=m_idx, color='g', label=f"Mean value ({dist[m_idx]})")
            axis[i, j].axvline(x=max_i, color='r', label=f"Max value ({max_v}, {max_i/1_000})")


    # Combine all the operations and display 
    plt.show() 

    # Save figures
    figure.savefig(f"Beta_dist_{len(sampler.numbers_of_rewards_1_ts)}.png")

    # Close plot
    plt.close(figure)

    # Log artifacts (output files)
    mlf.log_artifact(f"Beta_dist_{len(sampler.numbers_of_rewards_1_ts)}.png")




if __name__ == "__main__":
    # Setup mlflow run & description
    now = datetime.now()
    date_time = now.strftime("%m_%d_%Y_%H:%M:%S")
    run_description = f"""
### Thompson Sampling ROI MAB - {date_time}

This program performs base line comparisons of thompson sampling vs random 
selection for a multi armed bandit problem. The program allows the 
specification of the amount you're willing to spend based on the cost of the
strategies & reward.

To load the strategies, the program expects the file XXXXXX. This file 
should contain the "true rate of success", in this example it is the 
`conversion_rate` column, the `cost` of selecting this strategy & the 
`reward` given if the strategy is successful .In addition you can create a 
sample environment based off of the conversion rates given in each strategy.

This allows for a high number of rounds in a scenario similar to bootstrapping.

In addition to exploring the sampling techniques, the distributions learnt 
from the sampling are plotted and saved as an artifact. 

Program Parameters:
* The `--file` parameter specifies the strategy file to use.
simulations
stop
spend
eval-ts
* The `--simulations` specifies how many simulated data rows to create space
* The `--stop` specifies how to stop the simulation
* The `--spend` is used if stopping with regret `R` or `ROI`
* The `--eval-ts` specifies if the stopping evaluation should be based off of 
        the thompson sampling performance 
    """
    args = parser.parse_args()

    # open file and load strategies
    with open(args.file, 'r') as f:
        strategies = json.loads(f.read())

    tags = {
        'mlflow.note.content': run_description,
        'mlflow.runName': f"thompson_v_random_sampling_{date_time}"
    }

    with mlflow.start_run(tags=tags) as run:
        spend = args.spend
        N = args.simulations
        is_ts = args.eval_ts
        stoping = args.stop

        # Setup simulation environment
        env = Environment(strategies=strategies)
        env.simulate_env(N=N, rate_field='conversion_rate')
        sampler = Sampler(env=env, mlflow=mlflow)

        # Run simulated selection
        if stoping == 'N':
            sampler.run() 
            stop_msg = f"After {N:,} samples"
        elif stoping == 'R':
            sampler.run_restric_regret(spend=spend, is_ts=is_ts) 
            stop_msg = f"More than {spend} missed conversions"
        elif stoping == 'ROI':
            sampler.run_restric_regret_roi(spend=spend, is_ts=is_ts) 
            stop_msg = f"Stop after spending ${spend}"

        # Save params
        mlflow.log_params({
            "spend": spend,
            "simulated_trials": N,
            "evaluate_thompson": is_ts,
            "stoping": stop_msg
        })

        # Log beta dist plots
        plot_beta_dists(sampler, mlflow)

        # Computing sampler metrics
        relative_return, relative_return_roi = sampler.relative_return()
        abs_roi = abs(sampler.total_roi_rs - sampler.total_roi_ts)
        ts_crs = sampler.expected_crs(is_ts=True).tolist()
        rs_crs = sampler.expected_crs(is_ts=False).tolist()
        ts_max_idx = np.argmax(ts_crs)
        rs_max_idx = np.argmax(rs_crs)

        # Log strategies with thompson & random expected conversion rate 
        strategies = sampler.update_strategies()
        mlflow.log_dict(strategies, "expected_strategy_performance.json")

        # # Save Metrics
        mlflow.log_metrics({
            "ts_best_cr": ts_crs[ts_max_idx],
            #"ts_best_cr_name": sampler.env.strategies[ts_max_idx]['name'],
            "rs_best_cr": rs_crs[rs_max_idx],
            #"rs_best_cr_name": sampler.env.strategies[rs_max_idx]['name'],
            "rel_roc": relative_return,
            "rel_roi": relative_return_roi,
            "ts_roi": sampler.total_roi_ts,
            "rs_roi": sampler.total_roi_rs,
            "abs_roi": abs_roi,
            "trails": sampler.n,
        })

    print("Relative Return on Conversion: {:.2f} %".format(relative_return))
    print("Relative ROI: {:.2f} %".format(relative_return_roi))
    print(f"Number of trails: {sampler.n} ")

    print(f"Thompson Sampling ROI: ${sampler.total_roi_ts:,}")
    print(f"Random Sampling ROI: ${sampler.total_roi_rs:,}")
    print(f"Absolute ROI: ${abs_roi:,.2f}")
