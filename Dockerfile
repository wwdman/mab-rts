# MLFlow local docker build for demo.
FROM --platform=linux/arm64 ghcr.io/mlflow/mlflow:v2.7.1
LABEL author="Daniel Stratti <dstratti1@gmail.com>"
LABEL maintainer="Daniel Stratti <dstratti1@gmail.com>"

# Setup CV Site
WORKDIR /app
COPY . .

# install git
RUN apt-get update && apt-get install -y git

# Expose local dev port & build dev server
EXPOSE 8080
CMD mlflow ui --host 0.0.0.0 --port 8080

# Get available archs and build
# docker run --rm mplatform/mquery ghcr.io/mlflow/mlflow:v2.7.1
# docker build . -t ml-flow
# docker run -it -v $PWD:/app -p 8080:8080 --platform=linux/arm64 ml-flow
