'''
 sampler.py

 @ticket:	
 @date:		2023-10-05
 @auth:		Daniel < dstratti1@gmail.com >

 @desc: 
    Compare Random & Thompson Sampling methods on an environemnt of strategies
    This class tracts metrics like regret and reward in ordre to determine the 
    strategy with the highest conversion rate in the most cost efficent method
'''
from environment import Environment
import random
import numpy as np



class Sampler(object):

    def __init__(self, env=Environment(strategies=[]), mlflow=None):
        self.env = env
        
        # Record Selection
        self.strategies_selected_rs = []
        self.strategies_selected_ts = []

        # Record rewards in trails & ROI $
        self.total_reward_rs = 0
        self.total_reward_ts = 0
        self.total_roi_rs = 0
        self.total_roi_ts = 0

        # TODO: Remove these
        # Record number of success & failures in H0 & HA
        self.numbers_of_rewards_1_ts = [0] * self.env.d
        self.numbers_of_rewards_0_ts = [0] * self.env.d
        self.numbers_of_rewards_1_rs = [0] * self.env.d
        self.numbers_of_rewards_0_rs = [0] * self.env.d

        
        # TODO: Remove these
        # Record Regret
        self.rewards_strategies = [0] * self.env.d
        self.regret_ts = []
        self.regret_roi_ts = []
        self.regret_rs = []
        self.regret_roi_rs = []
        self.total_cost_bs = 0
        self.total_reward_bs = 0

        self.n = 0
        self.mlflow = mlflow


    def random_sample(self, row):
        strategy_rs = random.randrange(self.env.d)
        self.strategies_selected_rs.append(strategy_rs)
        reward_rs = self.env.X[row, strategy_rs]
        
        self.total_roi_rs -= self.env.strategies[strategy_rs]['cost'] # pay to play
        if reward_rs == 1:
            self.numbers_of_rewards_1_rs[strategy_rs] = self.numbers_of_rewards_1_rs[strategy_rs] + 1
            self.total_roi_rs += self.env.strategies[strategy_rs]['reward']
        else:
            self.numbers_of_rewards_0_rs[strategy_rs] = self.numbers_of_rewards_0_rs[strategy_rs] + 1
        
        
        self.total_reward_rs += reward_rs


        if self.mlflow is not None:
            # # Save Metrics
            self.mlflow.log_metric("rs_roi", self.total_roi_rs, step=row)



    def best_strategy(self, row):
        # Best strategy
        for i in range(0, self.env.d):
            self.rewards_strategies[i] = self.rewards_strategies[i] + self.env.X[row, i]
        self.total_reward_bs = max(self.rewards_strategies)
        
        bs_idx = np.argmax(self.env.X[row, i])
        bs_cost = self.env.strategies[bs_idx]['reward'] - self.env.strategies[bs_idx]['cost']
        self.total_cost_bs += bs_cost


    def regret(self):

        # Regret Random
        self.regret_rs.append(self.total_reward_bs - self.total_reward_rs)
        self.regret_roi_rs.append(self.total_cost_bs - self.total_roi_rs)
        
        # Regret Thompson
        self.regret_ts.append(self.total_reward_bs - self.total_reward_ts)
        self.regret_roi_ts.append(self.total_cost_bs - self.total_roi_ts)


    def thompson_sample(self, row):
        # Thompson Sampling
        strategy_ts = 0
        max_random = 0
        for i in range(0, self.env.d):
            # Beta distribution. Returns [0, 1] based on the number of success versus failures
            random_beta = random.betavariate(self.numbers_of_rewards_1_ts[i] + 1, self.numbers_of_rewards_0_ts[i] + 1)
            
            # Get best strategy based on the max beta value
            if random_beta > max_random:
                max_random = random_beta
                strategy_ts = i
        
        # record success & failures
        reward_ts = self.env.X[row, strategy_ts]
        
        self.total_roi_ts -= self.env.strategies[strategy_ts]['cost'] # pay to play
        if reward_ts == 1:
            self.numbers_of_rewards_1_ts[strategy_ts] = self.numbers_of_rewards_1_ts[strategy_ts] + 1
            self.total_roi_ts += self.env.strategies[strategy_ts]['reward']
        else:
            self.numbers_of_rewards_0_ts[strategy_ts] = self.numbers_of_rewards_0_ts[strategy_ts] + 1
        
        self.strategies_selected_ts.append(strategy_ts)
        self.total_reward_ts += reward_ts

        if self.mlflow is not None:
            # # Save Metrics
            self.mlflow.log_metric("ts_roi", self.total_roi_ts, step=row)
            



    def relative_return(self):

        relative_return = (self.total_reward_ts - self.total_reward_rs) / self.total_reward_rs * 100
        relative_return_roi = abs(self.total_roi_ts - self.total_roi_rs) / abs(self.total_roi_rs) * 100

        return relative_return, relative_return_roi


    def succes_rates(self, is_ts):

        if is_ts:
            success = np.array(numbers_of_rewards_1_ts)
            total = (success + np.array(numbers_of_rewards_0_ts))
        else:
            success = np.array(numbers_of_rewards_1_rs)
            total = (success + np.array(numbers_of_rewards_0_rs))

        rates = success / total
        return rates.round(2).tolist()

    def simulate(self):
        self.random_sample(self.n)
        
        # Best strategy
        self.best_strategy(self.n)
        
        # Thompson Sampling
        self.thompson_sample(self.n)
        self.regret()


    def run_restric_regret_roi(self, spend=1_000, is_ts=True):
        #for n in range(0, N):
        self.n = 0
        while (abs(self.total_roi_ts if is_ts else self.total_roi_rs) <= spend and self.n < self.env.N):
            self.simulate()
            self.n += 1

    def run_restric_regret(self, spend=20, is_ts=True):
        #for n in range(0, N):
        self.n = 0
        while self.total_reward_rs == 0 or ((self.regret_ts[-1] if is_ts else self.regret_rs[-1]) <= spend and self.n < self.env.N):
            self.simulate()
            self.n += 1


    def run(self, N=None):
        N = self.env.N if N is None else N

        for n in range(0, N):
            self.simulate()

        self.n = N


    #def 

    # Plot 9 Beta distribution, each representing either a progression in success of failures

    def beta_dist(self, alpha, beta, points=1000):
        '''
        Create a number line of 1000 datum representing the beta distribution given 
        a number of success / failed trails

        alpha : int
            Number of successes plus 1 as it cannot be 0

        beta : int
            Number of failures plus 1 as it cannot be 0
        '''
        c = 1 / ((math.factorial(alpha) * math.factorial(beta)) / (math.factorial(alpha + beta)))
        p_sup = alpha - 1
        p_msup = beta - 1
        
        dist = []
        for x in np.arange(0, 1, step=(1/points)):
            dist.append(c * (x**p_sup) * ((1 - x)**p_msup))
            
        return np.array(dist)


    def anova_betas(self, n_cat, points):

        betas = np.zeros((self.env.d, 1000))

        for i in range(0, self.env.d):
            # Beta distribution. Returns [0, 1] based on the number of success versus failures
            betas[i] = self.beta_dist(self.numbers_of_rewards_1_ts[i] + 1, self.numbers_of_rewards_0_ts[i] + 1)
            



    def bootstrap_crs(self, rounds=1_000):
        '''
        Run the thompson & random sampling multiple times to get the average
        expected performance of each strategy.
        '''
        ts_mean_cr = np.zeros((rounds, self.env.d))
        rs_mean_cr = np.zeros((rounds, self.env.d))
        for i in range(rounds):
            self.run()
            ts_mean_cr[i] = self.expected_crs()
            rs_mean_cr[i] = self.expected_crs(is_ts=False)


    def expected_crs(self, is_ts=True):
        if is_ts:
            return (np.array(self.numbers_of_rewards_1_ts)  / self.n)
        else:
            return (np.array(self.numbers_of_rewards_1_rs) / self.n)


    def update_strategies(self):
        ts_crs = self.expected_crs(is_ts=True)
        rs_crs = self.expected_crs(is_ts=False)

        strategies = self.env.strategies

        for i, r in enumerate(strategies):
            strategies[i]['ts_ecr'] = ts_crs[i]
            strategies[i]['rs_ecr'] = rs_crs[i]

        return strategies

    def reset(self):
        # Record Selection
        self.strategies_selected_rs = []
        self.strategies_selected_ts = []

        # Record rewards in trails & ROI $
        self.total_reward_rs = 0
        self.total_reward_ts = 0
        self.total_roi_rs = 0
        self.total_roi_ts = 0

        # TODO: Remove these
        # Record number of success & failures in H0 & HA
        self.numbers_of_rewards_1_ts = [0] * self.env.d
        self.numbers_of_rewards_0_ts = [0] * self.env.d
        self.numbers_of_rewards_1_rs = [0] * self.env.d
        self.numbers_of_rewards_0_rs = [0] * self.env.d

        
        # TODO: Remove these
        # Record Regret
        self.rewards_strategies = [0] * self.env.d
        self.regret_ts = []
        self.regret_roi_ts = []
        self.regret_rs = []
        self.regret_roi_rs = []
        self.total_cost_bs = 0
        self.total_reward_bs = 0

        self.n = 0

