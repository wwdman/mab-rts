'''
 environment.py

 @ticket:	
 @date:		2023-10-05
 @auth:		Daniel < dstratti1@gmail.com >

 @desc: 
    Simulate Trails¶
    Below creates 10,000 trails to simulate running these strategies. For this 
    simulated data we have a fixed conversion rate specifed below for each. In the 
    real world this would be sourced via actual experiments either live or from 
    focus groups.

    see: demo_strategies.json for expected JSON structure of strategy file.
'''
import numpy as np


class Environment(object):
    def __init__(self, strategies=[]):
        self.strategies = strategies
        self.d = len(self.strategies)

    def simulate_env(self, N=10_000, rate_field='conversion_rate'):
        '''
        Create simulated samples based off the initialised strategies. This will
        produce a matrix of N by d where d is the number of strategies populated
        with zeros representing no conversion and one representing a conversion.
        A single row can contain a conversion for more than one strategy

        N : int
                    Number of rows to simulate

        rate_field : string
                    The field in the strategies that represents the conversion 
                    rate to simulate.

        returns : np.array
                    A `N` by `d` matrix with each row representing 
        '''
        self.N = N
        self.X = np.array(np.zeros([self.N,self.d]))

        # Creating the simulation
        self.conversion_rates = [x[rate_field] for x in self.strategies]
        
        # For each round, randomly generate probability that user converts, 
        # if below stratergy conversion rate record as conversion
        for i in range(self.N):
            for j in range(self.d):
                if np.random.rand() <= self.conversion_rates[j]:
                    self.X[i,j] = 1