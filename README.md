# Multi Armed Bandit
In probability theory and machine learning, the multi-armed bandit problem (sometimes called the K- or N-armed bandit problem) is a problem in which a fixed limited set of resources must be allocated between competing (alternative) choices in a way that maximizes their expected gain, when each choice's properties are only partially known at the time of allocation, and may become better understood as time passes or by allocating resources to the choice

## Implementation
In this implementation, you can either generate simulated data based off known costs, reward & a **true conversion rate** or you can load in samples that you have recorded. We can then compare Thompson Sampling to Random Sampling for identifying the most profitable strategy. **Note** in real world implementation you would use Thompson Sampling to decide which strategy to inact, this program simply lets you hypothesis test and explore the expected cost of exploring these strategies.

After performing the simulations the program uses ML-Flow to store statistcs such as total cost, trails to converge and descriptive statistics on the distributions learnt (mean comparrison f-test)


## Local Development
To run this program please have docker installed. Once installed run the below command to build the image. Please note that this is assuming your running it on an Mac OS M2 chip.
```bash
# Get available archs and build if not on M2
# docker run --rm mplatform/mquery ghcr.io/mlflow/mlflow:v2.7.1
docker build . -t ml-flow

# change --platform=linux/arm64 to match your arch if available in the above commented out command
docker run -it -v $PWD:/app -p 8080:8080 --platform=linux/arm64 ml-flow
```

Once container is up and running with ML-Flow you will need to open a new terminal and connect to it so you can run python programs.
```bash
# list containers and their IDs
docker container ls

# get the ID from the above and replace <container-id>
docker exec -it <container-id> bash
```

Once in the container you con execute the below pyton program. This can either be run with a fixed ROI spend, based on missed conversions or just a number of simulations.

```bash
root@e14bb7d440c7:/app# python test_strategies.py -f demo_strategies.json -N 10000 -s N -ts
root@e14bb7d440c7:/app# python test_strategies.py \
	--file demo_strategies.json \
	--simulations 10000 \
	--stop N \
	--eval-ts
```

```bash
# Miss out on 1000 conversion and quit
root@e14bb7d440c7:/app# python test_strategies.py -f demo_strategies.json -N 10000 -s R -d 1000 -ts
root@e14bb7d440c7:/app# python test_strategies.py \
	--file demo_strategies.json \
	--simulations 10000 \
	--stop R \
	--spend 1000 \
	--eval-ts
```

```bash
# Miss out on $1000 conversion and quit
root@e14bb7d440c7:/app# python test_strategies.py -f demo_strategies.json -N 10000 -s ROI -d 1000 -ts
root@e14bb7d440c7:/app# python test_strategies.py \
	--file demo_strategies.json \
	--simulations 10000 \
	--stop ROI \
	--spend 1000 \
	--eval-ts
```

Now you should be able to run any program as you please. Additionally your current directory has been attached to the container so any updates to files you make on your local machine in this directory will be updated on the active container.

## Running

### Example Simulation
Once you have set up the two terminals (one running ML-Flow, the other BASH) you can simply run the below command to execute the example simulation. In this example there are 9 ad campaigns with vairos costs associated with running each, and various rewards if a each convert.
